import Vue from 'vue';

export const FooterEditLinkMixin = Vue.extend({
  created() {
    this.$nuxt.$emit('contentfulIds', {
      spaceId: this.spaceId,
      entryId: this.entryId,
    });
  },
});
