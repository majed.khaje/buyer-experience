## Goal
We would like to X which will improve Y as measured by Z.

#### Please provide more information related to this request
Make sure the information you provide is relevant for your request. If unsure, please provide all the fields. Add/remove rows as needed.

If the requestor is not the DRI, find out if the DRI is aware of the request / wants to change things.

**Does anyone in leadership have eyes on this project?**
- [ ]  Yes `@personshandle`
- [ ]  No

**Compliance / legal / accessibility regulations to be aware of?**
- [ ] Yes: `Please describe why`
- [ ] No: `Please describe why`

**How do we measure success?**
 - `Please describe how success will be measured`

## Page(s)
Which page(s) are involved in this request?
* `[Page Title](URL)`

## In scope
What is within scope of this request?

- [ ] Deliverable 1
- [ ] Deliverable 2

## DCI
[DRI, Consulted, Informed](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#dri-consulted-informed-dci)

- [ ] DRI: `GitLab Handle`
- [ ] Consulted: `GitLab Handle`
- [ ] Informed: `Everyone`

## Requirements
What are the requirements for this request? Checklist below is an example of common requirements, please check all that apply and adjust as necessary:

- [ ] Copy writing
- [ ] Illustration
- [ ] Custom Graphics
- [ ] Research
- [ ] Data / Analytics
- [ ] UX Design
- [ ] Engineering
- [ ] Marketing Ops ([New Marketo forms](https://handbook.gitlab.com/handbook/marketing/marketing-operations/campaign-operations/#working-with-us), etc.)
/label ~"dex-status::triage"
