# NOTE: Static Sites often want to optimize for fast build and deploy
#       pipeline times, so changes are published as quickly as possible.
#       Therefore, this default config includes optional variables, settings,
#       and caching, which help minimize job run times. For example,
#       disabling support for git LFS and submodules. There are also retry
#       and reliability settings which help prevent false build failures
#       due to occasional infrastructure availability problems. These are
#       all documented inline below, and can be changed or removed as
#       necessary, depending on the requirements for your repo or project.
include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
    rules:
      - if: $CI_COMMIT_BRANCH != "main"

stages:
  - prepare
  - lint
  - build
  - test
  - version
  - deploy

default:
  image: node:18.16-alpine
  interruptible: true # All jobs are interruptible by default
  # The following 'retry' configuration settings may help avoid false build failures
  # during brief problems with CI/CD infrastructure availability
  retry:
    max: 2 # This is confusing but this means "3 runs at max".
    when:
      - unknown_failure
      - api_failure
      - runner_system_failure
      - job_execution_timeout
      - stuck_or_timeout_failure

variables:
  PROJECT_PIPELINE_NAME: '$CI_COMMIT_MESSAGE'
  CTF_ENTRY_NAME: $CTF_ENTRY_NAME
  CTF_ENTRY_ID: $CTF_ENTRY_ID
  CTF_ENTRY_VERSION: $CTF_ENTRY_VERSION
  CTF_ENTRY_UPDATED_BY: $CTF_ENTRY_UPDATED_BY

  NODE_OPTIONS: --max_old_space_size=3584
  # NOTE: The following PERFORMANCE and RELIABILITY variables are optional, but may
  #       improve performance of larger repositories, or improve reliability during
  #       brief problems with CI/CD infrastructure availability

  ### PERFORMANCE ###
  # GIT_* variables to speed up repo cloning/fetching
  GIT_DEPTH: 10
  # Disabling LFS and submodules will speed up jobs, because runners don't have to perform
  # the submodule steps during repo clone/fetch. These settings can be deleted if you are using
  # LFS or submodules.
  GIT_LFS_SKIP_SMUDGE: 1
  GIT_SUBMODULE_STRATEGY: none
  FF_USE_FASTZIP: 'true'

  ### RELIABILITY ###
  # Reduce potential of flaky builds via https://docs.gitlab.com/ee/ci/yaml/#job-stages-attempts variables
  GET_SOURCES_ATTEMPTS: 3
  ARTIFACT_DOWNLOAD_ATTEMPTS: 3
  RESTORE_CACHE_ATTEMPTS: 3
  EXECUTOR_JOB_SECTION_ATTEMPTS: 3

### COMMON JOBS REUSED VIA `extends`:
.deployment-image:
  # Deployments need to use the Google Cloud SDK. This is why we split it out from the build job,
  # we used to use `google/cloud-sdk:latest`, but in order to patch gsutil to gzip files,
  # we are using the `www-gitlab-com` image. The imporant bit happens in this MR: https://gitlab.com/gitlab-org/gitlab-build-images/-/merge_requests/472,
  # which should allow us to complete the work in https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/122
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:www-gitlab-com-3.0

workflow:
  name: '$PROJECT_PIPELINE_NAME'
  rules:
    - if: '$CI_PIPELINE_SOURCE == "trigger"'
      variables:
        PROJECT_PIPELINE_NAME: 'Triggered by changes in CTF entry $CTF_ENTRY_ID'
    - when: always # Other pipelines can run, but use the default name

## Global Cache
cache:
  - key:
      files:
        - yarn.lock
    paths:
      - node_modules/
    policy: pull

## Anchors
.fallback_cache: &fallback_cache |
  if [[ -d node_modules ]]; then
    echo "Cached modules found. Using cache...";
  else
    echo "Cached modules not found. Installing modules...";
    yarn install --cache-folder .yarn-cache --frozen-lockfile --prefer-offline
  fi

###################################
#
# PREPARE STAGE
#
###################################
before_script:
  - apk add --update curl && rm -rf /var/cache/apk/*

install:
  stage: .pre
  cache:
    - key:
        files:
          - yarn.lock
      paths:
        - node_modules/
      when: on_success
      policy: pull-push # Update the cache
    - key: $CI_JOB_NAME
      paths:
        - .yarn-cache/
      when: on_success
      policy: pull-push
  script:
    - echo 'yarn-offline-mirror ".yarn-cache/"' >> .yarnrc
    - echo 'yarn-offline-mirror-pruning true' >> .yarnrc
    - yarn install --cache-folder .yarn-cache --frozen-lockfile --prefer-offline
  rules:
    - changes:
        - yarn.lock
      if: '$CI_PIPELINE_SOURCE != "trigger" && ($CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821" || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH)'

close-stale-issues:
  stage: .pre
  dependencies:
    - install
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "close_stale_issues"'
  script:
    - *fallback_cache
    - yarn close-stale-issues

###################################
#
# LINT STAGE
#
###################################

js:
  stage: lint
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
  script:
    - *fallback_cache
    - yarn lint:js

prettier:
  stage: lint
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
  script:
    - *fallback_cache
    - yarn lint:prettier

###################################
#
# BUILD STAGE
#
###################################

build:
  stage: build
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  artifacts:
    expire_in: 7 days
    paths:
      - dist
  script:
    - |
      if [ "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ]; then
        if echo "$CI_MERGE_REQUEST_DESCRIPTION" | grep -q "\- \[x\] Use Contentful Preview API"; then
          echo "Using Contentful Preview API";
          export USE_CONTENTFUL_PREVIEW_API='true'
        else
          echo "Using Contentful CDN API";
          export USE_CONTENTFUL_PREVIEW_API='false'
        fi
      else
        echo "Using Contentful CDN API";
        export USE_CONTENTFUL_PREVIEW_API='false'
      fi
    - *fallback_cache
    - yarn sync-data
    - yarn generate

###################################
#
# TEST STAGE
#
###################################

test:
  stage: test
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
  script:
    - *fallback_cache
    - yarn run test

log-contentful-information:
  stage: .pre
  rules:
    - if: '$CI_PIPELINE_SOURCE == "trigger"'
  script: |
    echo "Entry ID: $CTF_ENTRY_ID"
    echo "Entry Name (present only if the entry has a title field): $CTF_ENTRY_NAME"
    echo "Entry version: $CTF_ENTRY_VERSION"
    echo "Entry updated by user: $CTF_ENTRY_UPDATED_BY"
    echo "entry URL: https://app.contentful.com/spaces/$CTF_SPACE_ID/entries/$CTF_ENTRY_ID"

scan-for-broken-links:
  stage: test
  needs:
    - job: build
      artifacts: true
  dependencies:
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "link_checker"'
  script:
    - *fallback_cache
    - yarn find-broken-links

lighthouse:
  image: registry.gitlab.com/gitlab-ci-utils/lighthouse:latest
  stage: test
  needs:
    - job: build
      artifacts: true
  dependencies:
    - build
  script:
    - *fallback_cache
    - yarn lighthouse autorun --upload.serverBaseUrl=${LHCI_SERVER_URL} --upload.token=${LHCI_BUILD_TOKEN} --upload.basicAuth.username=${LHCI_BASIC_AUTH_USERNAME} --upload.basicAuth.password=${LHCI_BASIC_AUTH_PASSWORD} --config=lighthouserc.js
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "lighthouse"'

# Clean up stopped review app environments. Done once a week in a scheduled pipeline,
# deletes all stopped review apps (limited to 100 because of pagination).
delete_stopped_environments:
  image: alpine:latest
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "delete_stopped_review_apps"'
  stage: test
  script:
    - apk --update add curl jq
    - |
      stopped_envs=$(curl --silent --header "PRIVATE-TOKEN: $GITLAB_PROJECT_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/28847821/environments?state=stopped&per_page=100")

      # Parse environment IDs and delete each one
      echo "$stopped_envs" | jq -r '.[] | "\(.name) \(.external_url) \(.id)"' | while read -r name url env_id; do
        echo "Deleting environment: $name ($url)"
        curl --silent --request DELETE --header "PRIVATE-TOKEN: $GITLAB_PROJECT_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/28847821/environments/$env_id"
      done

###################################
#
# VERSION STAGE
#
###################################

version:
  stage: version
  before_script:
    - yarn get-week
    - apk add --no-cache git
    - apk add --update --no-cache curl
  needs:
    - job: build
      artifacts: true
  dependencies:
    - build
  script:
    - *fallback_cache
    - yarn semantic-release
  variables:
    GL_TOKEN: $GITLAB_PROJECT_ACCESS_TOKEN # GL_TOKEN is consumed within the semantic-release package (https://github.com/semantic-release/gitlab?tab=readme-ov-file#environment-variables)
  allow_failure: true
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "changelog"'

###################################
#
# DEPLOY STAGE
#
###################################

# Review app logic for merge requests.
review:
  stage: deploy
  extends: .deployment-image
  needs:
    - job: build
      artifacts: true
  # The build job is listed as a dependency, so we will always have its artifacts available here,
  # and we can focus solely on executing deployment logic for the review apps
  dependencies:
    - build
  rules:
    # Run this job on merge requests, ONLY for a branch in the original repo.
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
  variables:
    DEPLOY_TYPE: review # This variable is used in the `scripts/deploy` bash file to switch on deployment logic.
  # Creates the review app environment, named with the slug for the commit ref, along with a corresponding URL that should match the deploy logic.
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_COMMIT_REF_SLUG.about.gitlab-review.app
    auto_stop_in: 30 days
  script:
    - mv dist/ public # The artifacts from the build job we care about are built to `dist/` by default in Nuxt, move to `public/`
    - scripts/deploy

pages:
  stage: deploy
  rules:
    # Only run the deploy on the default branch. If you want to deploy from Merge Request branches,
    # you can use Review Apps: https://docs.gitlab.com/ee/ci/review_apps
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule") || ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $SCHEDULED_JOB == "changelog")
  needs:
    - build
  dependencies:
    - build
  script:
    - mv dist/ public
  artifacts:
    expire_in: 7 days
    paths:
      - public

production-deploy:
  stage: deploy
  extends: .deployment-image
  needs:
    - build
  # The build job is listed as a dependency, so we will always have its artifacts available here,
  # and we can focus solely on executing deployment logic
  dependencies:
    - build
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule") || ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $SCHEDULED_JOB == "changelog")
  variables:
    DEPLOY_TYPE: production # This variable is used in the `scripts/deploy` bash file to switch on deployment logic.
  script:
    - mv dist/ public # The artifacts from the build job we care about are built to `dist/` by default in Nuxt, move to `public/`
    - scripts/deploy
