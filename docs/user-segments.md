# Personalization by user segment

We can serve different content for the same component based in workplace size segmentation of users. There's only a limited number of components supported and the page might need customization in order to implement this feature properly.

## Contentful

List of segmented components:

- `components/home/customer-logos.vue` of content-type `Customer Logos`
- `components/home/tabbed-case-studies.vue` of content-type `Tab Controls Container`

List of pages serving segmented components:

- [Homepage](/)

### Contentful segment field

All segmented components have a `segment` field in Contentful. This allows us to choose define our entry as a an entry for said segment. Right now, we are segmenting users based on their employers segment (more on that in the [mixin section](#mixin)). This means that an entry for a segmented component **should** include the `segment` field. A segmented entry can only have the following segments defined in the `segment` field:

- `default` - The base version of the component for unidentified users.
- `ent` - For Entreprise
- `mm` - For Medium sized business
- `sb` - For Small business

### Contentful entries

When we segment a component, we multiply the number of entries present by the number of segments (fixed values in [USER_SEGMENTS](../common/constants.ts)) to identify the total number of entries **we need to have** in Contentful.

#### Important note on localized pages

If the page is not localized, the total number of entries for the segmented component in Contentful equals the number of defined segments.

However, if the page is localized, the total number of entries would need to be multiplied by number of supported languages (4 languages at this time).

## Mixin

The [UserSegmentsMixin](../mixins/user-segments.mixin.ts) loads the current `6sense` employer data for an identified user in the `beforeMount` method, selects the `employee_count` variable and sets it to `this.employeeCount` data property.

The mixin also creates a couple of methods for, first defining the segment based in the `employeeCount` in the `defineSegment()` method. This method is `private` in the sense that **it is not meant to be used outside the mixin**.

The second method is `setSegment(data: any, component: string)` which is meant to be used in the page we are segmentating. It takes an array of unkown data which only condition is that objects **must have** a root level property named `segment` with a segment value from [USER_SEGMENTS](../common/constants.ts).
This should be the segment field in Contentful wired and mapped all the way down to the page for it to be passed as transformed, cleaned array of data in the props for that page.

## In-page implementation

We need to wait until the component is mounted and the `window` browser object has loaded so we add an event listener to accomplish this inside the `mounted()` lifecycle method of our page.

Here's an example of the event listener in the homepage. We set an event listener to the `load` event of the `window` object. Inside of it we want to set our segments, which basically all it does is find the index of for each supported segment in the provided data.

```js
  mounted() {
    // Let's wait until the window is loaded to ensure data in mixin's beforeMount is available
    window.addEventListener('load', () => {
      // Each of these functions sets the appropriate segment and corresponding index used in the computed properties
      this.setSegment(
        this.doc?.personalized_customer_logos,
        COMPONENT_NAMES.CUSTOMER_LOGOS,
      );
      this.setSegment(
        this.doc?.personalized_tabbed_case_studies,
        COMPONENT_NAMES.TABBED_CASE_STUDIES,
      );
    });
  },
```

To do this, call the `setSegment()` function provided by the mixin with two arguments, the array of segmented components and the name of the component. After this, you will have an index for each supported segment. 

In this case, we will have `segmentCustomersIndex` and `segmentCasesIndex` available in our page. Both these values represent the position of the active segment component in the array of similar components.

## Computed properties for the components

Having set the indexes where the segmented component is placed whitin the array of components provided, we can use them inside computed properties that will finally provided the correct data to the component.

Remember, we haven't changed how our component is built and implemented. Instead, we are filtering data from a set of options and providing the result to the component, to be implemented as usual but with segmented data.

This use case fits nicely into the computed properties provided by `vue`, where we use our indexes to filter the data.

Continuing with our previous example, the corresponding computed properties will look like something like this:

```js
  computed: {
    // Both segmented components data needs to be filtered
    caseStudiesToRender() {
      // Property this.segmentCasesIndex is set in the UserSegmentsMixin
      return {
        ...this.doc?.personalized_tabbed_case_studies[this.segmentCasesIndex]
          .quotes_carousel_block,
      };
    },
    customerLogosToRender() {
      // Property this.segmentCustomersIndex is set in the UserSegmentsMixin
      return {
        ...this.doc?.personalized_customer_logos[this.segmentCustomersIndex],
      };
    },
  },
```

The computed properties on the page will have access to the segment indexes calculated in the mixin.

Notice that we already have a clean `personalized` array of components from which we can select the active segment, with a little help from our segment indexes.

## Segment `segment` query param personalization

We can also have cases where we want to create personalized URLs for each segment. Wide range of uses cases for this feature, could include personalized lead generations or testing segments in development enviroments.

If we want to go the personalized page for the small business `segment`, we make sure the URL includes the `segment` query param. A typical URL would look like:

```bash
https://about.gitlab.com/?segment=sb
```
