---
  title: L'innersource avec GitLab
  description: L'innersource est l'adoption des meilleures pratiques de l'open source et la mise en place d'une culture de l'open source au sein d'une entreprise. En savoir plus!
  template: 'industry'
  no_gradient: true
  nextstep_variant: 5
  components:
    - name: 'solutions-hero'
      data:
        title: L'innersource avec GitLab
        subtitle: Un portail développeur intégré pour exploiter les meilleures pratiques et processus open source afin de travailler et de collaborer plus efficacement.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/stock3.png
          image_url_mobile: /nuxt-images/solutions/stock3.png
          alt: "Image: GitLab pour le secteur public"
          bordered: true
    - name: 'side-navigation-variant'
      links:
        - title: Avantages
          href: '#benefits'
        - title: Capacités
          href: '#capabilities'
        - title: Tarifs
          href: '#pricing'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Pourquoi adopter l'innersource ?
                subtitle: |
                  Les équipes de développement et d'ingénierie hautement performantes adoptent des processus et une culture open source afin d'améliorer l'expérience des développeurs et la collaboration. Les équipes utilisent GitLab comme un portail de développement interne pour:
                link:
                    text: En savoir plus sur l'innersource
                    href: https://about.gitlab.com/topics/version-control/what-is-innersource/
                    ga_name: innersource topic
                    ga_location: benefits
                is_accordion: true
                items:
                  - icon:
                      name: speed-alt-2
                      alt: Icône speed-alt-2
                      variant: marketing
                    header: Compilez des logiciels de meilleure qualité, plus rapidement
                    text: Les tests unitaires, la couverture de code, l'intégration continue et la sécurité de bout en bout intégrée contribuent à renforcer et à améliorer le code en amont.
                  - icon:
                      name: docs-alt
                      alt: Icône de documents
                      variant: marketing
                    header: Créer une documentation complète
                    text: Le code est mieux documenté dans les requêtes de fusion grâce aux commentaires et aux modifications suggérées, ainsi que dans les wikis, les tickets et les README pour les processus
                  - icon:
                      name: stopwatch
                      alt: Icône de chronomètre
                      variant: marketing
                    header: Gagnez en efficacité
                    text: Le code, l'architecture et les ressources sont détectables et disponibles dans toutes les équipes et au sein de l'entreprise
                  - icon:
                      name: user-collaboration
                      alt: Icône de collaboration entre utilisateurs
                      variant: marketing
                    header: Améliorer la collaboration
                    text: Réduction des frictions lors de la revue de code, des communications et des contributions au sein des équipes et entre les équipes à l'échelle de l'entreprise, et tout cela sur une seule plateforme.
                  - icon:
                      name: user-laptop
                      alt: Icône d'ordinateur portable avec un utilisateur au centre
                      variant: marketing
                    header: Améliorer l'expérience des membres de l'équipe
                    text: Des silos décomposés, une productivité et un bonheur améliorés et en plus de cela, une meilleure rétention de l'équipe et un meilleur recrutement    
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'solutions-by-solution-list'
              data:
                title: L'innersource avec GitLab
                icon: slp-check-circle-alt
                header: "GitLab permet aux équipes de développement de logiciels d'améliorer la qualité, de gagner en efficacité, d'accroître la collaboration et d'accélérer l'innovation, en offrant une expérience développeur simplifiée et agréable.\_ Cette expérience est optimisée par l'approche unique de la plateforme DevSecOps de GitLab avec un portail développeur interne intégré comprenant :"
                items: 
                  - Une documentation des processus et des produits sous la forme de README et de runbooks personnalisables
                  - Des revues de code centralisées et collaboratives par le biais de [requêtes de fusion](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge request"data-ga-location="capabilities"}
                  - La gestion des connaissances à l'aide des [Wikis](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wikis" data-ga-location="capabilities"}
                  - Des idées, des communications et des mises à jour via l'utilisation de [tickets](https://docs.gitlab.com/ee/user/project/issues){data-ga-name="issues" data-ga-location="capabilities"} et de [fils de discussion](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="threaded discussions" data-ga-location="capabilities"}
                  - Documentation API complète
                  - Visualisation des résultats de la couverture de test
                  - Web IDE puissant basé sur VS Code, hébergé directement dans l'UI
                  - Interface de ligne de commande (ILC) native
                footer: Le tout hébergé sur la même plateforme où les équipes travaillent, de la planification jusqu'à la production.
                cta:
                  text: Découvrez comment libérer la puissance de l'innersource
                  video: https://www.youtube.com/embed/ZS1mCpBHXaI
                  ga_name: innersource unlock
                  ga_location: capabilities
                  
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Quel forfait vous convient le mieux ?
                cta:
                  url: /pricing/
                  text: En savoir plus sur la tarification
                  data_ga_name: pricing learn more
                  data_ga_location: pricing
                  aria_label: tarifs
                tiers:
                  - id: free
                    title: Forfait Gratuit
                    items:
                      - SAST et détection des secrets
                      - Résultats dans le fichier json
                  - id: premium
                    title: Premium
                    items:
                      - Toutes les fonctionnalités du forfait Gratuit, plus
                      - Approbations et garde-fous
                    link:
                      href: /pricing/premium/
                      text: Découvrir l'édition Premium
                      data_ga_name: premium learn more
                      data_ga_location: pricing
                      aria_label: édition premium
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Tout ce qui compose l'édition Premium, plus
                      - DAST, test à données aléatoires des API, conformité des licences, etc.
                      - SBOM et listes des dépendances
                      - Gestion des vulnérabilités
                      - Gestion de la conformité
                    link:
                      href: /pricing/ultimate
                      text: En savoir plus sur GitLab Ultimate
                      data_ga_name: ultimate learn more
                      data_ga_location: pricing
                      aria_label: édition ultimate
                    cta:
                      href: /free-trial/
                      text: Essayer Ultimate gratuitement
                      data_ga_name: try ultimate
                      data_ga_location: pricing
                      aria_label: édition ultimate
      