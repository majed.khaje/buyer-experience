---
  title: KubernetesをDevOpsライフサイクルにインテグレーションする
  description: GitLabのKubernetesインテグレーションを使用すれば、アプリのビルド、テスト、デプロイ、実行を大規模に行えるようになります。
  components:
    - name: 'solutions-hero'
      data:
        title: Kubernetes + GitLab
        subtitle: アプリのビルド、テスト、デプロイ、実行に必要な機能がすべて揃っています
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /webcast/scalable-app-deploy/
          text: チームがアプリのデプロイをスケールするのにGitLabがどのように役立つかを確認
          data_ga_name: scale app deployment
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像: Kubernetes + GitLab"
    - name: copy-media
      data:
        block:
          - header: "クラウドネイティブ開発に最適なソリューション"
            miscellaneous: |

              クラウドネイティブアプリケーションは、ソフトウェア開発の未来を形成します。コンテナ化および動的管理がなされ、マイクロサービス指向であるクラウドネイティブシステムを使用することで、オペレーションの安定性を維持しながら、開発速度を上げることができます。

              GitLabは、[エンドツーエンドのソフトウェア開発およびオペレーション](/stages-devops-lifecycle/){data-ga-name="devops lifecycle" data-ga-location="body"}において必要な機能をすべてを備えた単一アプリケーションです。イシューの追跡やソースコード管理のほか、CI/CD、モニタリングまで、すべての作業を1か所で行えるため、複雑なツールチェーンを簡素化し、サイクルタイムを短縮できます。[ビルトインのコンテナレジストリ](https://docs.gitlab.com/ee/user/packages/container_registry/index.html){data-ga-name="container registry" data-ga-location="body"}と[Kubernetesインテグレーション](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"}を備えたGitLabを活用すれば、コンテナ化やクラウドネイティブ開発をこれまで以上に簡単に開始できるようになり、さらにクラウドアプリの開発プロセスも最適化できます。
          - header: Kubernetesとは？
            miscellaneous: |
              Kubernetesは、オープンソースのコンテナオーケストレーションプラットフォームです。アプリケーションコンテナの管理における作業全体（デプロイやスケーリングからオペレーションまで）を自動化することを目的として設計されています。Kubernetesオーケストレーションを使用すれば、必要に応じてスケールアップおよびスケールダウンを行いながら、パーティション化を実行できるようになります。本番環境内のハードウェア使用率を制限し、機能のロールアウト時における中断を最小限に抑えながら、顧客の要求に迅速かつ効率的に対応できます。
            media_link_href: /blog/2017/11/30/containers-kubernetes-basics/
            media_link_text: Kubernetesの詳細について
            media_link_data_ga_name: more about kubernetes
            media_link_data_ga_location: body
          - header: Kubernetes上でGitLabをデプロイするか、GitLabを使用してKubernetes上でソフトウェアのテストとデプロイを行う
            miscellaneous: |
              GitLabをKubernetesと連携させる、またはKubernetes内で動作させるには、3つの異なる方法があります。これらの方法はすべて、単独で使用することも、組み合わせて使用することもできます。

              * [GitLabからKubernetesにソフトウェアをデプロイする](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="deploy with kubernetes" data-ga-location="body"}
              * [Kubernetesを使用してGitLabインスタンスに紐づいたRunnerを管理する](https://docs.gitlab.com/runner/install/kubernetes.html){data-ga-name="kubernetes runners" data-ga-location="body"}
              * [GitLabのアプリケーションとサービスをKubernetesクラスター上で実行する](https://docs.gitlab.com/charts/){data-ga-name="kubernetes cluster" data-ga-location="body"}

              上記の手法は、組み合わせて使用したり、単独で使用したりできます。例えば、仮想マシン上で実行されているオムニバスGitLabインスタンスの場合、その中に保管されているソフトウェアをDocker Runnerを通じてKubernetesにデプロイできます。
          - header: Kubernetesインテグレーション
            inverted: true
            text: |
              GitLabを使用すれば、ベアメタルから仮想マシンまで、ほぼどこにでもアプリをデプロイできますが、GitLabはKubernetes用に設計されています。[Kubernetesインテグレーション](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"} を使用すれば、次のような高度な機能を利用できるようになります。

              * [プルベースのデプロイ](https://docs.gitlab.com/ee/user/clusters/agent/repository.html#synchronize-manifest-projects){data-ga-name="pull-based deployments" data-ga-location="body"}
              * [安全な接続を介したGitLab CI/CDからのデプロイ](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html){data-ga-name="CI/CD secure connection" data-ga-location="body"}
              * [カナリアデプロイ](https://docs.gitlab.com/ee/user/project/canary_deployments.html){data-ga-name="canary deployments" data-ga-location="body"}
              * [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/){data-ga-name="auto devops" data-ga-location="body"}
            icon:
              use_local_icon: true
              name: /nuxt-images/logos/kubernetes.svg
              alt: Kubernetes
          - header: GitLabを使用してKubernetes上でアプリのテストとデプロイを行う
            text: |
              [GitLab CI/CD](/features/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"}を使用すれば、複数の環境へのデプロイを簡単に管理できるようになります。また、オートスケールの[GitLab Runner](https://docs.gitlab.com/runner/){data-ga-name="gitlab runners" data-ga-location="body"}と自動テストを同時に実行できます。さらに、Review Appsでコードをマージする前に、本番環境のようなライブ環境で変更を手動でテストすることも可能です。Runnerや[Reviews Apps](/stages-devops-lifecycle/review-apps/){data-ga-name="review apps" data-ga-location="body"}、および独自のアプリケーションを、Kubernetesクラスターや任意の環境にデプロイすることもできます。
            icon:
              name: rocket-alt
              alt: ロケット
              variant: marketing
              hex_color: '#F43012'
            link_href: /partners/technology-partners/google-cloud-platform/
            link_text: Google Cloud Platformにデプロイ
            link_data_ga_name: deploy on GCP
            link_data_ga_location: body
    - name: featured-media
      data:
        column_size: 3
        horizontal_rule: true
        media:
          - title: Auto DevOpsドキュメント
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-devops.png
              alt: "DevOps"
            link:
              text: 詳細はこちら
              href: https://docs.gitlab.com/ee/topics/autodevops/index.html
              data_ga_name: DevOps documentation
              data_ga_location: body
          - title: 自動デプロイによるCI/CDパイプラインの作成
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm.png
              alt: "CI/CDパイプラインの作成に関するブログ投稿"
            link:
              text: 詳細はこちら
              href: /blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
              data_ga_name: CI/CD with auto deploy
              data_ga_location: body
          - title: KubernetesにGitLabをインストール
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-gitlab-kubernetes.png
              alt: "CI/CDパイプラインの作成に関するブログ投稿"
            link:
              text: 詳細はこちら
              href: https://docs.gitlab.com/charts/
              data_ga_name: install gitlab on kubernetes
              data_ga_location: body
          - title: クラウドネイティブに関するウェビナー
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-webcast.png
              alt: "GitLabウェブキャスト"
            link:
              text: 今すぐ視聴
              href: /blog/2017/04/18/cloud-native-demo/
              data_ga_name: cloud native webinar
              data_ga_location: body
