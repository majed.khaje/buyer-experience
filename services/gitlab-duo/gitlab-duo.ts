import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { CtfCard, CtfEntry } from '~/models';

export function mapGitlabDuo(items) {
  let [
    hero,
    journey,
    video,
    featureCards,
    principles,
    resources,
    reportCta,
    // eslint-disable-next-line prefer-const
    benefits,
  ] = items[0].fields.pageContent;
  //  Format content from contentful
  // Hero content
  hero = {
    button: {
      href: hero?.fields?.primaryCta?.fields?.externalUrl,
      text: hero?.fields?.primaryCta?.fields?.text,
      dataGaName: hero?.fields?.primaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.primaryCta?.fields?.dataGaLocation,
      variant: 'secondary',
    },
    secondaryButton: hero?.fields?.secondaryCta?.fields?.text && {
      href: hero?.fields?.secondaryCta?.fields?.externalUrl,
      text: hero?.fields?.secondaryCta?.fields?.text,
      dataGaName: hero?.fields?.secondaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.secondaryCta?.fields?.dataGaLocation,
      variant: 'primary',
    },
    description: hero?.fields?.subheader,
    image: {
      alt: hero?.fields?.backgroundImage?.fields?.title,
      src: hero?.fields?.backgroundImage?.fields?.file?.url,
    },
    ...hero.fields.customFields,
  };

  // Journey cards content

  journey = {
    features: journey?.fields?.data?.features,
  };

  // Video

  video = {
    text: video?.fields?.title,
    source: video?.fields?.url,
  };

  // Feature cards

  featureCards = {
    title: featureCards?.fields?.header,
    cards: featureCards?.fields?.card?.map((card: any) => ({
      name: card?.fields?.title,
      description: card?.fields?.description,
      href: card?.fields?.cardLink,
      icon: card?.fields?.iconName,
      data_ga_location: card?.fields?.cardLinkDataGaLocation,
      data_ga_name: card?.fields?.cardLinkDataGaName,
    })),
  };

  // Principles

  principles = principles?.fields?.data;

  //  Resources

  resources = {
    data: {
      column_size: 4,
      title: resources?.fields?.header,
      cards: resources?.fields?.card?.map((card: any) => ({
        link_text: 'Read more',
        data_ga_location: 'body',
        event_type: 'Blog',
        data_ga_name: card?.fields?.title,
        header: card?.fields?.title,
        href: card?.fields?.cardLink,
        icon: {
          name: card?.fields?.iconName,
          variant: 'marketing',
          alt: 'blog icon',
        },
        image: card?.fields?.image?.fields?.file?.url,
      })),
    },
  };

  //  Report CTA

  reportCta = {
    layout: 'dark',
    title: reportCta?.fields?.header,
    reports: reportCta?.fields?.card?.map((report: any) => ({
      url: report?.fields?.button?.fields?.externalUrl,
      description: report?.fields?.description,
      data_ga_name: report?.fields?.cardLinkDataGaName,
      data_ga_location: report?.fields?.cardLinkDataGaLocation,
    })),
  };

  const metadata = {
    title: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    ogTitle: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    description: items[0]?.fields?.seoMetadata[0]?.fields?.description,
    ogDescription: items[0]?.fields?.seoMetadata[0]?.fields?.ogDescription,
    ogType: items[0]?.fields?.seoMetadata[0]?.fields?.ogType,
    ogUrl: items[0]?.fields?.seoMetadata[0]?.fields?.ogUrl,
    ogSiteName: items[0]?.fields?.seoMetadata[0]?.fields?.ogSiteName,
    ogImage: items[0]?.fields?.seoMetadata[0]?.fields?.ogImage,
  };
  return {
    hero,
    journey,
    video,
    featureCards,
    principles,
    resources,
    reportCta,
    metadata,
    benefits,
  };
}

async function grabPricingCards(): Promise<CtfEntry<CtfCard>[]> {
  try {
    const queries = [
      getClient().getEntries({
        content_type: CONTENT_TYPES.CARD,
        'sys.id': '3uwIEJT93Q7DMkDGYrZgnM',
        include: 4,
      }),
      getClient().getEntries({
        content_type: CONTENT_TYPES.CARD,
        'sys.id': '3zMrhwWAMWosvf6kPzihg3',
        include: 4,
      }),
    ];

    const results = await Promise.all(queries);
    return results.map((result) => result.items[0]);
  } catch (e) {
    throw new Error(e);
  }
}

export async function mapGitlabDuoEnglishContent(app) {
  const pricing = (await grabPricingCards()).map((item) =>
    app.$pricingService.mapFeaturedAddonCard(item),
  );

  return {
    pricing,
  };
}
